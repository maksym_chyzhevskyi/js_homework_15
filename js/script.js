/* Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?
Рекурсія дозволяє функції викликати саму себе.
Дозволяє виконувати однотипні операції, скорочує розмір коду.
*/

let num = +prompt('Enter a number greater than zero');
while (isNaN(num) || num === 0) {
  num = +prompt('Enter a number greater than zero', num);
}

function getFactorial(num) {
  return (num != 1) ? num * getFactorial(num - 1) : 1;
}

alert(getFactorial(num));



